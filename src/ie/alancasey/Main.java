package ie.alancasey;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        TicTacToe ticTacToeGame = new TicTacToe();

        while(!ticTacToeGame.isGameFinished()){
            System.out.println();
            ticTacToeGame.printBoard();
            System.out.println("Player " + ticTacToeGame.getCurrentPlayerMark()
                    + ", its your turn. Enter your move (row[0-2] column[0-2]): ");

            ticTacToeGame.placeMark(in.nextInt(), in.nextInt());
        }

        ticTacToeGame.printBoard();
        if(!ticTacToeGame.checkForWin()){
            System.out.println("This game was a draw, please try again.");
        } else {
            System.out.println("Player " + ticTacToeGame.getWinningPlayerMark() + " won the game. Congratulations!!");
        }

    }
}
