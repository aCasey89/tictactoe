package ie.alancasey;


class TicTacToe {

    private char [][] board = new char[3][3];
    private char currentPlayerMark = 'X';
    private char winningPlayerMark = '-';

    TicTacToe(){
        this.initialiseBoard();
    }

    void printBoard(){
        System.out.println("-------------");
        for(int i = 0; i < 3; i++){
            System.out.print("| ");
            for(int j = 0; j < 3; j++){
                System.out.print(this.board[i][j]);
                System.out.print(" | ");
            }
            System.out.println("");
        }
        System.out.println("-------------");
    }

    boolean placeMark(final int x, final int y){
        if(isGameFinished()) return false;

        if(!checkIfIndexValid(x, y)){
            System.out.println("That is not a valid index please try again.");
            return false;
        } else {
            this.board[x][y] = this.currentPlayerMark;
            this.toggleCurrentPlayer();
            return true;
        }
    }

    boolean isGameFinished() {
        return (checkForWin() || checkIsBoardFull());
    }

    boolean checkForWin(){
        return (checkRowsForWin() || checkColumnsForWin() || checkDiagonalsForWin());
    }

    boolean checkIsBoardFull(){
        boolean boardFull = true;
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                if(this.board[i][j] == '-') boardFull = false;
            }
        }

        return boardFull;
    }

    char getCurrentPlayerMark() {
        return currentPlayerMark;
    }

    char getWinningPlayerMark() {
        return winningPlayerMark;
    }

    private boolean checkRowsForWin(){
        for(int i = 0; i < 3; i++){
            if(checkForWin(this.board[0][i], this.board[1][i], this.board[2][i])) return true;
        }

        return false;
    }

    private boolean checkColumnsForWin(){
        for(int i = 0; i < 3; i++){
            if(checkForWin(this.board[i][0], this.board[i][1], this.board[i][2])) return true;
        }

        return false;
    }

    private boolean checkDiagonalsForWin(){
        return (checkForWin(this.board[0][0], this.board[1][1], this.board[2][2]))
                || (checkForWin(this.board[2][0], this.board[1][1], this.board[0][2]));
    }

    private boolean checkForWin(final char char1, final char char2, final char char3){
        if(char1 != '-' && char1 == char2 && char2 == char3){
            this.winningPlayerMark = char1;
            return true;
        } else {
            return false;
        }
    }

    private void initialiseBoard(){
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                this.board[i][j] = '-';
            }
        }
    }

    private boolean checkIfIndexValid(final int x, final int y){
        return x >= 0 && x < 3 && y >= 0 && y < 3 && this.board[x][y] == '-';
    }

    private void toggleCurrentPlayer(){
        if(this.currentPlayerMark == 'X'){
            this.currentPlayerMark = 'O';
        } else {
            this.currentPlayerMark = 'X';
        }
    }
}
