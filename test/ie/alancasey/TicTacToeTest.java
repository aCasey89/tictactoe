package ie.alancasey;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TicTacToeTest {

    private TicTacToe ticTacToe;

    @Before
    public void setup(){
        this.ticTacToe = new TicTacToe();
    }

    @Test
    public void testPlaceMarkTrue(){
        Assert.assertEquals('X', this.ticTacToe.getCurrentPlayerMark());
        Assert.assertTrue(this.ticTacToe.placeMark(2, 2));
        Assert.assertEquals('O', this.ticTacToe.getCurrentPlayerMark());
        Assert.assertTrue(this.ticTacToe.placeMark(1, 1));
        Assert.assertEquals('X', this.ticTacToe.getCurrentPlayerMark());
    }

    @Test
    public void testPlaceMarkFalse(){
        Assert.assertEquals('X', this.ticTacToe.getCurrentPlayerMark());
        Assert.assertTrue(this.ticTacToe.placeMark(2, 2));
        Assert.assertEquals('O', this.ticTacToe.getCurrentPlayerMark());
        Assert.assertFalse(this.ticTacToe.placeMark(2, 2));
        Assert.assertEquals('O', this.ticTacToe.getCurrentPlayerMark());
    }

    @Test
    public void testRowWin(){
        Assert.assertTrue(this.ticTacToe.placeMark(0, 0));
        Assert.assertTrue(this.ticTacToe.placeMark(2, 2));
        Assert.assertTrue(this.ticTacToe.placeMark(0, 1));
        Assert.assertTrue(this.ticTacToe.placeMark(2, 1));
        Assert.assertFalse(this.ticTacToe.isGameFinished());
        Assert.assertTrue(this.ticTacToe.placeMark(0, 2));
        Assert.assertTrue(this.ticTacToe.isGameFinished());
        Assert.assertEquals('X', this.ticTacToe.getWinningPlayerMark());
    }

    @Test
    public void testColWin(){
        Assert.assertTrue(this.ticTacToe.placeMark(0, 0));
        Assert.assertTrue(this.ticTacToe.placeMark(2, 2));
        Assert.assertTrue(this.ticTacToe.placeMark(1, 0));
        Assert.assertFalse(this.ticTacToe.isGameFinished());
        Assert.assertTrue(this.ticTacToe.placeMark(2, 1));
        Assert.assertTrue(this.ticTacToe.placeMark(2, 0));
        Assert.assertTrue(this.ticTacToe.isGameFinished());
        Assert.assertEquals('X', this.ticTacToe.getWinningPlayerMark());
    }

    @Test
    public void testDiagonalWin(){
        Assert.assertTrue(this.ticTacToe.placeMark(0, 0));
        Assert.assertTrue(this.ticTacToe.placeMark(1, 2));
        Assert.assertFalse(this.ticTacToe.isGameFinished());
        Assert.assertTrue(this.ticTacToe.placeMark(1, 1));
        Assert.assertTrue(this.ticTacToe.placeMark(2, 1));
        Assert.assertTrue(this.ticTacToe.placeMark(2, 2));
        Assert.assertTrue(this.ticTacToe.isGameFinished());
        Assert.assertEquals('X', this.ticTacToe.getWinningPlayerMark());
    }
}
